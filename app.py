from fastapi import FastAPI, Body, HTTPException, status
from fastapi.responses import Response, JSONResponse
from fastapi.encoders import jsonable_encoder
from typing import List

from db import db
from model import PostModel, UpdatePostModel, ListPostModel


app = FastAPI()


@app.post("/", response_description="Add new post", response_model=PostModel)
async def create_post(post: PostModel = Body(...)):
    post = jsonable_encoder(post)
    new_post = await db["posts"].insert_one(post)
    created_post = await db["posts"].find_one({"_id": new_post.inserted_id})
    return JSONResponse(status_code=status.HTTP_201_CREATED, content=created_post)


@app.get(
    "/", response_description="List all posts", response_model=List[ListPostModel]
)
async def list_posts():
    posts = await db["posts"].find({}, {"title": 1, "_id": 1, "short_description": 1}).to_list(
        await db.posts.count_documents({}))
    return posts


@app.get(
    "/{id}", response_description="Get a single post", response_model=PostModel
)
async def show_post(id: str):
    if (post := await db["posts"].find_one({"_id": id})) is not None:
        return post

    raise HTTPException(status_code=404, detail=f"Post {id} not found")


@app.put("/{id}", response_description="Update a post", response_model=PostModel)
async def update_post(id: str, post: UpdatePostModel = Body(...)):
    post = {k: v for k, v in post.dict().items() if v is not None}

    if len(post) >= 1:
        update_result = await db["posts"].update_one({"_id": id}, {"$set": post})

        if update_result.modified_count == 1:
            if (
                    updated_post := await db["posts"].find_one({"_id": id})
            ) is not None:
                return updated_post

    if (existing_post := await db["posts"].find_one({"_id": id})) is not None:
        return existing_post

    raise HTTPException(status_code=404, detail=f"Post {id} not found")


@app.delete("/{id}", response_description="Delete a post")
async def delete_post(id: str):
    delete_result = await db["posts"].delete_one({"_id": id})

    if delete_result.deleted_count == 1:
        return Response(status_code=status.HTTP_204_NO_CONTENT)

    raise HTTPException(status_code=404, detail=f"Post {id} not found")


