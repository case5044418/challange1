import asyncio
from fastapi.testclient import TestClient
from app import app

test_client = TestClient(app)


def test_list():
    response = test_client.get("/")
    assert response.status_code == 200


def test_create():
    response = test_client.post("/", json={})
    assert response.status_code == 422


async def test_detail():
    await asyncio.sleep(0.5)
    response = test_client.get("/123")
    assert response.status_code == 404


async def test_delete():
    await asyncio.sleep(0.5)
    response = test_client.delete("/123")
    assert response.status_code == 404


def test_put():
    response = test_client.put("/123")
    assert response.status_code == 422
