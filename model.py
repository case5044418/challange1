import datetime
from typing import Optional

from bson import ObjectId
from pydantic import BaseModel, Field, root_validator


class PyObjectId(ObjectId):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, v):
        if not ObjectId.is_valid(v):
            raise ValueError("Invalid objectid")
        return ObjectId(v)

    @classmethod
    def __modify_schema__(cls, field_schema):
        field_schema.update(type="string")


class PostModel(BaseModel):
    id: PyObjectId = Field(default_factory=PyObjectId, alias="_id")
    title: str
    short_description: str = Field(..., max_length=200)
    description: str
    tags: list
    created_at: datetime.datetime = datetime.datetime.now()
    updated_at: datetime.datetime = datetime.datetime.now()

    class Config:
        allow_population_by_field_name = True
        arbitrary_types_allowed = True
        validate_assignment = True
        json_encoders = {ObjectId: str}

    @root_validator
    def updated_at_validator(cls, values):
        values["updated_at"] = datetime.datetime.now()
        return values


class UpdatePostModel(BaseModel):
    title: Optional[str]
    short_description: Optional[str]
    description: Optional[str]
    tags: Optional[list[str]]

    class Config:
        arbitrary_types_allowed = True
        json_encoders = {ObjectId: str}


class ListPostModel(BaseModel):
    id: PyObjectId = Field(default_factory=PyObjectId, alias="_id")
    title: str
    short_description: str = Field(..., max_length=200)

    class Config:
        arbitrary_types_allowed = True
        json_encoders = {ObjectId: str}
