# What is this?

This project includes simple post registration, updating, deleting, listing and fetching current post's detail APIs.


## Installation

- Use the package manager [pip](https://pip.pypa.io/en/stable/) to install requirement files or you can install all files
directly via requirements.txt

```bash
pip install -r requirements.txt
```

## Usage

- Download the repository
- Install the required packages
- Edit inside .env file
- Run server ```uvicorn app:app --reload```



### List API

Endpoint: {{url}}/

Method: GET

Response:
```json
[
 {
        "_id": "6450ec21caafcc6235e39abe",
        "title": "test2",
        "short_description": "short_description2"
    },
    {
        "_id": "6450ec2acaafcc6235e39abf",
        "title": "test1",
        "short_description": "short_description1"
    }
]
```

### Create API

Endpoint: {{url}}/

Method: POST

Request:
```json
{
    "title":"test1",
    "short_description":"short_description1",
    "description":"description description description",
    "tags":["a","b"]
}
```

Response:
```json
{
    "_id": "6450fd604d1b44ec1acbf903",
    "title": "test1",
    "short_description": "short_description1",
    "description": "description description description",
    "tags": [
        "a",
        "b"
    ],
    "created_at": "2023-05-02T15:02:36.344121",
    "updated_at": "2023-05-02T15:09:04.095279"
}
```


### Update API

Endpoint: {{url}}/<_id>/

Method: PUT

Request:
```json
{
    "short_description": "new short_description"
}
```

Response:
```json
{
    "_id": "6450fd604d1b44ec1acbf903",
    "title": "test1",
    "short_description": "new short_description",
    "description": "description description description",
    "tags": [
        "a",
        "b"
    ],
    "created_at": "2023-05-02T15:02:36.344121",
    "updated_at": "2023-05-02T15:10:00.860584"
}
```

### Detail Post API

Endpoint: {{url}}/<_id>/

Method: GET

Response:
```json
{
    "_id": "6450fd604d1b44ec1acbf903",
    "title": "test1",
    "short_description": "123123asdasdas33333d2323asd2a",
    "description": "description description description",
    "tags": [
        "a",
        "b"
    ],
    "created_at": "2023-05-02T15:02:36.344121",
    "updated_at": "2023-05-02T15:10:00.860584"
}
```

### DELETE API

Endpoint: {{url}}/<_id>/

Method: DELETE

Reponse Status Code: 204